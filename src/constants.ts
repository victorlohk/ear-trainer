export const letters = [
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B',
]

export const isChromatic = (letterNumber: number) => {
  return [1, 3, 6, 8, 10].includes(letterNumber)
}

export const Solfèges = [
  'Do',
  'Ra',
  'Re',
  'Me',
  'Mi',
  'Fa',
  'Fi',
  'So',
  'Si',
  'La',
  'Li',
  'Ti',
]

export const intervals = [
  { name: 'Perfect Unison', distance: 0 },
  { name: 'Minor Second', distance: 1 },
  { name: 'Major Second', distance: 2 },
  { name: 'Minor Third', distance: 3 },
  { name: 'Major Third', distance: 4 },
  { name: 'Perfect Fourth', distance: 5 },
  { name: 'Tritone', distance: 6 },
  { name: 'Perfect Fifth', distance: 7 },
  { name: 'Minor Sixth', distance: 8 },
  { name: 'Major Sixth', distance: 9 },
  { name: 'Minor Seventh', distance: 10 },
  { name: 'Major Seventh', distance: 11 },
  { name: 'Perfect Octave', distance: 12 },
]