import { useEffect, useState } from 'react'
import * as Tone from 'tone'
import { letters, intervals, Solfèges, isChromatic } from '../constants.js'
import { Piano } from '@tonejs/piano'

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const NoteListening = () => {
  const [letterNumber, setLetterNumber] = useState(-1);
  const [showAnswer, setShowAnswer] = useState(false);
  const [started, setStarted] = useState(false);
  const [includeChromatic, setIncludeChromatic] = useState(false);
  const [needCadence, setNeedCadence] = useState(true);
  const [isLoading, setIsLoading] = useState(true)


  let now: number = Tone.now()

  const piano = new Tone.Sampler({
    urls: {
      C4: "C4.mp3",
      "D#4": "Ds4.mp3",
      "F#4": "Fs4.mp3",
      A4: "A4.mp3",
    },
    release: 1,
    baseUrl: "https://tonejs.github.io/audio/salamander/",
  }).toDestination();

  useEffect(() => {
    Tone.loaded().then(() => setIsLoading(false))
  })


  const playCadence = () => {
    const increment = 0.5
    const duration = 0.3

    now = Tone.now()
    piano.triggerAttackRelease(["C4", "E4", "G4"], duration, now);
    piano.triggerAttackRelease(["C4", "F4", "A4"], duration, now + increment);
    piano.triggerAttackRelease(["B3", "D4", "G4"], duration, now + 2 * increment);
    piano.triggerAttackRelease(["C4", "E4", "G4"], duration, now + 3 * increment);
    now = now + 4 * increment + 0.3;
  }

  const genNextQuestion = () => {
    if (needCadence) playCadence()
    setShowAnswer(false)

    let letterNumber: number
    do {
      letterNumber = Math.floor(Math.random() * letters.length)
    } while (!includeChromatic && isChromatic(letterNumber))

    setLetterNumber(letterNumber) // pick random letter from letters)
    playNote(letterNumber)
  }

  const playNote = (letterNumber: number) => {
    const letter = letters[letterNumber]

    if (now < Tone.now()) {
      now = Tone.now()
    }

    piano.triggerAttackRelease(letter + "4", "8n", now + 0.4);
    console.log({ letter });
    now = now + 1.2
  }

  const repeat = () => {
    now = Tone.now()
    if (needCadence) playCadence()
    playNote(letterNumber)
  }

  return (
    <div>
      {(isLoading) && "Loading..."}
      {
        (!started && !isLoading) ?
          <button onClick={(e) => { setStarted(true); genNextQuestion() }}>Start</button>
          : null
      }
      {(started) ?
        <div>
          <div>
            <label>
              Chromatic?
              <input type="checkbox" style={{ transform: "scale(1.5)", marginLeft: "7px" }}
                checked={includeChromatic}
                onChange={() => { setIncludeChromatic(!includeChromatic) }}
              />
            </label>
          </div>
          <div>
            <label>
              Cadence?
              <input type="checkbox" style={{ transform: "scale(1.5)", marginLeft: "7px" }}
                checked={needCadence}
                onChange={() => { setNeedCadence(!needCadence) }}
              />
            </label>
          </div>
          <div>
            <button onClick={(e) => { genNextQuestion() }}>Next Question</button>
            <button onClick={(e) => { repeat() }}>Repeat</button>
          </div>
          <div><button onClick={(e) => { setShowAnswer(true) }}>Show Answer</button></div>
          <p>
            {showAnswer && (
              `Answer: ${letters[letterNumber]} (${Solfèges[letterNumber]})`
              ?? '')
            }
            {!showAnswer && '---'}
          </p>
        </div>

        : null
      }
    </div>
  );
}

export default NoteListening;